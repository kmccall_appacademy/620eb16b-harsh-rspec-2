require 'time'

def measure(n=1, &prc)
  t1=Time.now
  i=0
  while i<n
    prc.call
    i+=1
  end
  t2=Time.now
  (t2-t1)/n
end
