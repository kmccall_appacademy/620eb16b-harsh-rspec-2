def reverser(&prc)
  prc.call.split.map {|word| word.reverse}.join(" ")
end

def adder(num=1, &prc)
  num + prc.call
end

def repeater(num=1, &prc)
  i=0
  while i < num
    prc.call
    i+=1
  end
end
